/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nogroup";

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
