/* See LICENSE file for license details. */
#define _XOPEN_SOURCE 500
#if HAVE_SHADOW_H
#include <shadow.h>
#endif

#include <ctype.h>
#include <errno.h>
#include <grp.h>
#include <pwd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <X11/extensions/Xrandr.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xinerama.h>
#include <sys/stat.h>
#include <dirent.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

#include <yaml.h>

#include <time.h>

#include "arg.h"
#include "util.h"

char *argv0;
GLuint textureHandle;

enum {
    INIT,
    INPUT,
    FAILED,
    NUMCOLS
};

struct lock {
    int screen;
    Window root, win;
    Pixmap pmap;
    unsigned long colors[NUMCOLS];
    XVisualInfo *vi;
    XineramaScreenInfo *xin_info;
    int xin_info_count;
    Colormap cmap;
    XSetWindowAttributes swa;
    GLXContext glc;
};

struct xrandr {
    int active;
    int evbase;
    int errbase;
};

#include "config.h"

static void
die(const char *errstr, ...) {
    va_list ap;

    va_start(ap, errstr);
    vfprintf(stderr, errstr, ap);
    va_end(ap);
    exit(1);
}

#ifdef __linux__
#include <fcntl.h>
#include <linux/oom.h>

static void
dontkillme(void)
{
    FILE *f;
    const char oomfile[] = "/proc/self/oom_score_adj";

    if (!(f = fopen(oomfile, "w"))) {
        if (errno == ENOENT)
            return;
        die("slock: fopen %s: %s\n", oomfile, strerror(errno));
    }
    fprintf(f, "%d", OOM_SCORE_ADJ_MIN);
    if (fclose(f)) {
        if (errno == EACCES)
            die("slock: unable to disable OOM killer. "
                "Make sure to suid or sgid slock.\n");
        else
            die("slock: fclose %s: %s\n", oomfile, strerror(errno));
    }
}
#endif

static const char *
gethash(void) {
    const char *hash;
    struct passwd *pw;

    /* Check if the current user has a password entry */
    errno = 0;
    if (!(pw = getpwuid(getuid()))) {
        if (errno)
            die("slock: getpwuid: %s\n", strerror(errno));
        else
            die("slock: cannot retrieve password entry\n");
    }
    hash = pw->pw_passwd;

#if HAVE_SHADOW_H
    if (!strcmp(hash, "x")) {
        struct spwd *sp;
        if (!(sp = getspnam(pw->pw_name)))
            die("slock: getspnam: cannot retrieve shadow entry. "
                "Make sure to suid or sgid slock.\n");
        hash = sp->sp_pwdp;
    }
#else
    if (!strcmp(hash, "*")) {
#ifdef __OpenBSD__
        if (!(pw = getpwuid_shadow(getuid())))
            die("slock: getpwnam_shadow: cannot retrieve shadow entry. "
                "Make sure to suid or sgid slock.\n");
        hash = pw->pw_passwd;
#else
        die("slock: getpwuid: cannot retrieve shadow entry. "
            "Make sure to suid or sgid slock.\n");
#endif /* __OpenBSD__ */
    }
#endif /* HAVE_SHADOW_H */

    return hash;
}

const float colors[6][3] = {{0, 0, 1},
                            {0, 1, 0},
                            {1, 0, 0},
                            {1, 1, 0},
                            {1, 0, 1},
                            {0, 1, 1}};
const int block_size = 50;

struct key_node {
    int key_down_x;
    int key_down_y;
    int key_down_color;
    int screen;
    float key_down_brightness;
    struct key_node *next;
};

struct the_runs {
    int running;
    int failure;
    int key_downs;
    int nscreens;
    struct key_node *head;
    struct lock **locks;
    Display *dpy;
    GC *gc;
};

// Function load a image, turn it into a texture, and return the texture ID as a GLuint for use
GLuint loadImage(const char *theFileName) {
    ILuint imageID;                // Create an image ID as a ULuint

    GLuint textureID;            // Create a texture ID as a GLuint

    ILboolean success;            // Create a flag to keep track of success/failure

//    ILenum error;                // Create a flag to keep track of the IL error state

    ilGenImages(1, &imageID);        // Generate the image ID

    ilBindImage(imageID);            // Bind the image

    success = ilLoadImage(theFileName);    // Load the image file

    // If we managed to load the image, then we can start to do things with it...
    if (success) {
        // If the image is flipped (i.e. upside-down and mirrored, flip it the right way up!)
        struct ILinfo ImageInfo;
        iluGetImageInfo(&ImageInfo);

        // Convert the image into a suitable format to work with
        // NOTE: If your image contains alpha channel you can replace IL_RGB with IL_RGBA
        success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);

        // Quit out if we failed the conversion
        if (!success) {
//            error = ilGetError();
            exit(-1);
        }

        // Generate a new texture
        glGenTextures(1, &textureID);

        // Bind the texture to a name
        glBindTexture(GL_TEXTURE_2D, textureID);

        // Set texture clamping method
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

        // Set texture interpolation method to use linear interpolation (no MIPMAPS)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        // Specify the texture specification
        glTexImage2D(GL_TEXTURE_2D,                // Type of texture
                     0,                // Pyramid level (for mip-mapping) - 0 is the top level
                     ilGetInteger(
                             IL_IMAGE_FORMAT),    // Internal pixel format to use. Can be a generic type like GL_RGB or GL_RGBA, or a sized type
                     ilGetInteger(IL_IMAGE_WIDTH),    // Image width
                     ilGetInteger(IL_IMAGE_HEIGHT),    // Image height
                     0,                // Border width in pixels (can either be 1 or 0)
                     ilGetInteger(IL_IMAGE_FORMAT),    // Format of image pixel data
                     GL_UNSIGNED_BYTE,        // Image data type
                     ilGetData());            // The actual image data itself
    } else {
//        error = ilGetError();
        printf("Could not load texture %s\n", theFileName);
        exit(-1);
    }

    ilDeleteImages(1,
                   &imageID); // Because we have already copied image data into texture data we can release memory used by image.

    return textureID; // Return the GLuint to the texture so you can use it!
}

void draw(struct the_runs *config) {
    for (int screen = 0; screen < config->nscreens; screen++) {
        XWindowAttributes gwa;
        XGetWindowAttributes(config->dpy, config->locks[screen]->win, &gwa);
        glViewport(0, 0, gwa.width, gwa.height);
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, gwa.width, gwa.height, 0, 1., 20.);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(0., 0., 10., 0., 0., 0., 0., 1., 0.);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glColor3f(1, 1, 1);
        glBindTexture(GL_TEXTURE_2D, textureHandle);
        glEnable(GL_TEXTURE_2D);
        if (config->locks[screen]->xin_info_count > 0) {
            for (int i = 0; i < config->locks[screen]->xin_info_count; i++) {
                glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex3f(config->locks[screen]->xin_info[i].x_org, config->locks[screen]->xin_info[i].y_org,
                           0); // Top Left
                glTexCoord2f(0, 1);
                glVertex3f(config->locks[screen]->xin_info[i].x_org,
                           config->locks[screen]->xin_info[i].y_org + config->locks[screen]->xin_info[i].height,
                           0); // Bottom Left
                glTexCoord2f(1, 1);
                glVertex3f(config->locks[screen]->xin_info[i].x_org + config->locks[screen]->xin_info[i].width,
                           config->locks[screen]->xin_info[i].y_org + config->locks[screen]->xin_info[i].height,
                           0); // Bottom Right
                glTexCoord2f(1, 0);
                glVertex3f(config->locks[screen]->xin_info[i].x_org + config->locks[screen]->xin_info[i].width,
                           config->locks[screen]->xin_info[i].y_org, 0); // Top Right
                glEnd();
            }
        } else {
            glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex3f(0, 0, 0); // Top Left
            glTexCoord2f(0, 1);
            glVertex3f(0, gwa.height, 0); // Bottom Left
            glTexCoord2f(1, 1);
            glVertex3f(gwa.width, gwa.height, 0); // Bottom Right
            glTexCoord2f(1, 0);
            glVertex3f(gwa.width, 0, 0); // Top Right
            glEnd();
        }
        glDisable(GL_TEXTURE_2D);

        for (struct key_node *node = config->head; node != NULL; node = node->next) {
            if (node->screen == screen) {
                const float *color = colors[node->key_down_color];
                float brightness = node->key_down_brightness;
                glColor4f(color[0], color[1], color[2], brightness);
                glBegin(GL_QUADS);
                glVertex3f(node->key_down_x, node->key_down_y, 0.01);
                glVertex3f(node->key_down_x, node->key_down_y + block_size, 0.01);
                glVertex3f(node->key_down_x + block_size, node->key_down_y + block_size, 0.01);
                glVertex3f(node->key_down_x + block_size, node->key_down_y, 0.01);
                glEnd();
            }
        }
        glDisable(GL_BLEND);
        glXSwapBuffers(config->dpy, config->locks[screen]->win);
    }
}

static void
readpw(Display *dpy, struct xrandr *rr, struct lock **locks, int nscreens,
       const char *hash) {
    XRRScreenChangeNotifyEvent *rre;
    char buf[32], passwd[256], *inputhash;
    int num, screen;
    struct the_runs *config = malloc(sizeof(struct the_runs));
    config->running = 1;
    config->failure = 0;
    config->nscreens = nscreens;
    config->locks = locks;
    config->dpy = dpy;
    config->gc = malloc(sizeof(GC) * nscreens);
    config->head = NULL;
    XGCValues values[nscreens];
    for (int i = 0; i < nscreens; i++) {
        config->gc[i] = XCreateGC(config->dpy, config->locks[i]->win, 0, &values[i]);
    }

    unsigned int len;
    KeySym ksym;
    XEvent ev;

    len = 0;

    while (config->running) {
        if (XPending(config->dpy)) {
            XNextEvent(config->dpy, &ev);
            if (ev.type == KeyPress) {
                for (screen = 0; screen < config->nscreens; screen++) {
                    if (config->locks[screen]->xin_info_count > 0) {
                        for (int i = 0; i < config->locks[screen]->xin_info_count; i++) {
                            struct key_node *node = malloc(sizeof(struct key_node));
                            node->key_down_x =
                                    ((rand() % config->locks[screen]->xin_info[i].width / block_size) * block_size) +
                                    config->locks[screen]->xin_info[i].x_org;
                            node->key_down_y =
                                    ((rand() % config->locks[screen]->xin_info[i].height / block_size) * block_size) +
                                    config->locks[screen]->xin_info[i].y_org;
                            node->key_down_color = rand() % 6;
                            node->key_down_brightness = 1.0;
                            node->screen = screen;
                            node->next = config->head;
                            config->head = node;
                        }
                    } else {
                        struct key_node *node = malloc(sizeof(struct key_node));
                        node->key_down_x =
                                (rand() % (WidthOfScreen(ScreenOfDisplay(config->dpy, screen)) / block_size)) *
                                block_size;
                        node->key_down_y =
                                (rand() % (HeightOfScreen(ScreenOfDisplay(config->dpy, screen)) / block_size)) *
                                block_size;
                        node->key_down_color = rand() % 6;
                        node->key_down_brightness = 1.0;
                        node->screen = screen;
                        node->next = config->head;
                        config->head = node;
                    }
                }
                explicit_bzero(&buf, sizeof(buf));
                num = XLookupString(&ev.xkey, buf, sizeof(buf), &ksym, 0);
                if (IsKeypadKey(ksym)) {
                    if (ksym == XK_KP_Enter)
                        ksym = XK_Return;
                    else if (ksym >= XK_KP_0 && ksym <= XK_KP_9)
                        ksym = (ksym - XK_KP_0) + XK_0;
                }
                if (IsFunctionKey(ksym) ||
                    IsKeypadKey(ksym) ||
                    IsMiscFunctionKey(ksym) ||
                    IsPFKey(ksym) ||
                    IsPrivateKeypadKey(ksym))
                    continue;
                switch (ksym) {
                    case XK_Return:
                        passwd[len] = '\0';
                        errno = 0;
                        if (!(inputhash = crypt(passwd, hash)))
                            fprintf(stderr, "slock: crypt: %s\n", strerror(errno));
                        else
                            config->running = !!strcmp(inputhash, hash);
                        if (config->running) {
                            XBell(dpy, 100);
                            config->failure = 1;
                        }
                        explicit_bzero(&passwd, sizeof(passwd));
                        len = 0;
                        break;
                    case XK_Escape:
                        explicit_bzero(&passwd, sizeof(passwd));
                        len = 0;
                        break;
                    case XK_BackSpace:
                        if (len)
                            passwd[--len] = '\0';
                        break;
                    default:
                        if (num && !iscntrl((int) buf[0]) &&
                            (len + num < sizeof(passwd))) {
                            memcpy(passwd + len, buf, num);
                            len += num;
                        }
                        break;
                }
            } else if (rr->active && ev.type == rr->evbase + RRScreenChangeNotify) {
                rre = (XRRScreenChangeNotifyEvent * ) & ev;
                for (screen = 0; screen < config->nscreens; screen++) {
                    if (config->locks[screen]->win == rre->window) {
                        if (rre->rotation == RR_Rotate_90 ||
                            rre->rotation == RR_Rotate_270)
                            XResizeWindow(config->dpy, config->locks[screen]->win,
                                          rre->height, rre->width);
                        else
                            XResizeWindow(config->dpy, config->locks[screen]->win,
                                          rre->width, rre->height);
//                        XClearWindow(config->dpy, config->locks[screen]->win);
                        break;
                    }
                }
            } else {
                for (screen = 0; screen < config->nscreens; screen++)
                    XRaiseWindow(config->dpy, config->locks[screen]->win);
            }

        }
        draw(config);
        struct key_node *node = config->head;
        while (node) {
            node->key_down_brightness -= 0.01;
            node = node->next;
        }
        while (config->head && config->head->key_down_brightness <= 0) {
            node = config->head;
            config->head = config->head->next;
            free(node);
        }
    }
    glXMakeCurrent(dpy, None, NULL);
    for (int i = 0; i < nscreens; i++) {
        glXDestroyContext(dpy, locks[i]->glc);
        XDestroyWindow(dpy, locks[i]->win);
    }
    for (int i = 0; i < nscreens; i++) {
        XFreeGC(dpy, config->gc[i]);
    }
    free(config->gc);
    struct key_node *node = config->head;
    while (config->head != NULL) {
        config->head = config->head->next;
        free(node);
        node = config->head;
    }
    free(config);
    XCloseDisplay(dpy);
}

static struct lock *
lockscreen(Display *dpy, struct xrandr *rr, int screen, XineramaScreenInfo *xin_info, int xin_info_count) {
    char curs[] = {0, 0, 0, 0, 0, 0, 0, 0};
    int i, ptgrab, kbgrab;
    struct lock *lock;
    XColor color;
    Cursor invisible;

    if (dpy == NULL || screen < 0 || !(lock = malloc(sizeof(struct lock))))
        return NULL;

    lock->screen = screen;
    lock->root = RootWindow(dpy, lock->screen);

    /* init */
    GLint att[] = {GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None};
    lock->vi = glXChooseVisual(dpy, 0, att);
    lock->xin_info = xin_info;
    lock->xin_info_count = xin_info_count;
    lock->cmap = XCreateColormap(dpy, lock->root, lock->vi->visual, AllocNone);

    lock->swa.colormap = lock->cmap;
    lock->swa.override_redirect = 1;
    lock->swa.event_mask = ExposureMask | KeyPressMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask;

    lock->win = XCreateWindow(dpy, lock->root, 0, 0,
                              DisplayWidth(dpy, lock->screen),
                              DisplayHeight(dpy, lock->screen),
                              0, lock->vi->depth,
                              InputOutput,
                              lock->vi->visual,
                              CWOverrideRedirect | CWColormap | CWEventMask | ExposureMask, &lock->swa);
    lock->glc = glXCreateContext(dpy, lock->vi, NULL, GL_TRUE);
    glXMakeCurrent(dpy, lock->win, lock->glc);
    lock->pmap = XCreateBitmapFromData(dpy, lock->win, curs, 8, 8);
    invisible = XCreatePixmapCursor(dpy, lock->pmap, lock->pmap, &color, &color, 0, 0);
    XDefineCursor(dpy, lock->win, invisible);

    /* Try to grab mouse pointer *and* keyboard for 600ms, else fail the lock */
    for (i = 0, ptgrab = kbgrab = -1; i < 6; i++) {
        if (ptgrab != GrabSuccess) {
            ptgrab = XGrabPointer(dpy, lock->root, False,
                                  ButtonPressMask | ButtonReleaseMask |
                                  PointerMotionMask, GrabModeAsync,
                                  GrabModeAsync, None, invisible, CurrentTime);
        }
        if (kbgrab != GrabSuccess) {
            kbgrab = XGrabKeyboard(dpy, lock->root, True,
                                   GrabModeAsync, GrabModeAsync, CurrentTime);
        }

        /* input is grabbed: we can lock the screen */
        if (ptgrab == GrabSuccess && kbgrab == GrabSuccess) {
            XMapRaised(dpy, lock->win);
            if (rr->active)
                XRRSelectInput(dpy, lock->win, RRScreenChangeNotifyMask);

            XSelectInput(dpy, lock->root, SubstructureNotifyMask);
            return lock;
        }

        /* retry on AlreadyGrabbed but fail on other errors */
        if ((ptgrab != AlreadyGrabbed && ptgrab != GrabSuccess) ||
            (kbgrab != AlreadyGrabbed && kbgrab != GrabSuccess))
            break;

        usleep(100000);
    }

    /* we couldn't grab all input: fail out */
    if (ptgrab != GrabSuccess)
        fprintf(stderr, "slock: unable to grab mouse pointer for screen %d\n",
                screen);
    if (kbgrab != GrabSuccess)
        fprintf(stderr, "slock: unable to grab keyboard for screen %d\n",
                screen);


    if (lock->vi == NULL) {
        printf("\n\tno appropriate visual found\n\n");
        exit(1);
    }
    return NULL;
}

static void
usage(void) {
    die("usage: slock [-v] [cmd [arg ...]]\n");
}

int read_handler(void *ext, unsigned char *buffer, size_t size, size_t *length) {
    int error = 0;
    return error ? 0 : 1;
}

int
main(int argc, char **argv) {

    char path[PATH_MAX];
    char texture_path[PATH_MAX];
    texture_path[0] = '\0';
    strcat(strcpy(path, getenv("HOME")), "/.config/slock.yaml\0");
    FILE *config_file = fopen(path, "rb");
    if (config_file) {
        yaml_parser_t parser;
        yaml_event_t event;
        yaml_parser_initialize(&parser);
        yaml_parser_set_input_file(&parser, config_file);
        int done = 0;
        int reading_path = 0;
        while (!done) {
            if (!yaml_parser_parse(&parser, &event)) {
                die("Failed to parse yaml config\n\t%s\n", path);
                done = 1;
            } else {
                if (event.type == YAML_SCALAR_EVENT) {
                    if (reading_path && event.data.scalar.value != NULL) {
                        strcpy(texture_path, (char *)event.data.scalar.value);
                    } else if (!strcmp((char *)event.data.scalar.value, "image_path")) {
                        reading_path = 1;
                    }
                }
                done = (event.type == YAML_STREAM_END_EVENT);
            }
            yaml_event_delete(&event);
        }
        yaml_parser_delete(&parser);
        fclose(config_file);
    }
    struct xrandr rr;
    struct lock **locks;
    struct passwd *pwd;
    struct group *grp;
    uid_t duid;
    gid_t dgid;
    const char *hash;
    Display *dpy;
    int s, nlocks, nscreens;
    srand(time(NULL));

    ARGBEGIN {
                case 'v':
                    fprintf(stderr, "slock-"VERSION"\n");
                    return 0;
                default:
                    usage();
            }ARGEND

    /* validate drop-user and -group */
    errno = 0;
    if (!(pwd = getpwnam(user)))
        die("slock: getpwnam %s: %s\n", user,
            errno ? strerror(errno) : "user entry not found");
    duid = pwd->pw_uid;
    errno = 0;
    if (!(grp = getgrnam(group)))
        die("slock: getgrnam %s: %s\n", group,
            errno ? strerror(errno) : "group entry not found");
    dgid = grp->gr_gid;

#ifdef __linux__
    dontkillme();
#endif

    hash = gethash();
    errno = 0;
    if (!crypt("", hash))
        die("slock: crypt: %s\n", strerror(errno));

    if (!(dpy = XOpenDisplay(NULL)))
        die("slock: cannot open display\n");

    /* drop privileges */
    if (setgroups(0, NULL) < 0)
        die("slock: setgroups: %s\n", strerror(errno));
    if (setgid(dgid) < 0)
        die("slock: setgid: %s\n", strerror(errno));
    if (setuid(duid) < 0)
        die("slock: setuid: %s\n", strerror(errno));

    /* check for Xrandr support */
    rr.active = XRRQueryExtension(dpy, &rr.evbase, &rr.errbase);

    /* get number of screens in display "dpy" and blank them */
    nscreens = ScreenCount(dpy);
    if (!(locks = calloc(nscreens, sizeof(struct lock *))))
        die("slock: out of memory\n");
    for (nlocks = 0, s = 0; s < nscreens; s++) {
        XineramaScreenInfo *info = NULL;
        int xin_number = s;
        if (XineramaIsActive(dpy)) {
            info = XineramaQueryScreens(dpy, &xin_number);
        }
        if ((locks[s] = lockscreen(dpy, &rr, s, info, xin_number)) != NULL)
            nlocks++;
        else
            break;
    }
    XSync(dpy, 0);
    glEnable(GL_DEPTH_TEST);

    /* did we manage to lock everything? */
    if (nlocks != nscreens)
        return 1;

    /* run post-lock command */
    if (argc > 0) {
        switch (fork()) {
            case -1:
                die("slock: fork failed: %s\n", strerror(errno));
            case 0:
                if (close(ConnectionNumber(dpy)) < 0)
                    die("slock: close: %s\n", strerror(errno));
                execvp(argv[0], argv);
                fprintf(stderr, "slock: execvp %s: %s\n", argv[0], strerror(errno));
                _exit(1);
        }
    }
    ilutRenderer(ILUT_OPENGL);
    ilInit();
    iluInit();
    ilutInit();
    ilutRenderer(ILUT_OPENGL);


    if (strlen(texture_path) > 0) {
        struct stat path_stat;
        if (stat(texture_path, &path_stat) == 0) {
            if (S_ISDIR(path_stat.st_mode)) {
                DIR *dp;
                struct dirent *ep;
                dp = opendir (texture_path);
                char texture_file_path[PATH_MAX];
                int total = 0;
                if (dp != NULL) {
                    while ((ep = readdir (dp))) {
                        strcpy(texture_file_path, texture_path);
                        strcat(texture_file_path, ep->d_name);
                        if (stat(texture_file_path, &path_stat) == 0) {
                            if (!S_ISDIR(path_stat.st_mode)) {
                                total++;
                            }
                        }
                    }
                    (void) closedir (dp);
                    int chosen = rand() % total;
                    total = 0;
                    dp = opendir (texture_path);
                    while ((ep = readdir (dp))) {
                        strcpy(texture_file_path, texture_path);
                        strcat(texture_file_path, ep->d_name);
                        if (stat(texture_file_path, &path_stat) == 0) {
                            if (!S_ISDIR(path_stat.st_mode)) {
                                total++;
                                if (total == chosen) {
                                    textureHandle = loadImage(texture_file_path);
                                    break;
                                }
                            }
                        }
                    }
                    (void) closedir (dp);
                } else {
                    printf("WARNING: Could not read from image_path\n");
                }
            } else {
                // Try to read the texture.
                textureHandle = loadImage(texture_path);
            }
        } else {
            printf("WARNING: image_path does not exist..\n");
        }
    }

    /* everything is now blank. Wait for the correct password */
    readpw(dpy, &rr, locks, nscreens, hash);
    for (int i = 0; i < nscreens; i++) {
        XFree(locks[i]->vi);
        if (locks[i]->xin_info != NULL)
            XFree(locks[i]->xin_info);
        free(locks[i]);
    }
    free(locks);
    return 0;
}
